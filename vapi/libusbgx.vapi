[CCode (cprefix = "usbg_", cheader_filename = "usbg/usbg.h")]
namespace USBG {
    [Compact]
	[CCode (cname = "usbg_state", cprefix = "usbg_", has_type_id = false)]
    public class State {
        public static USBG.Error init (string configfs_path, out unowned USBG.State? state);
        public void cleanup ();
        public unowned string get_configfs_path ();
        public unowned USBG.Gadget? get_gadget (string name);
        public unowned USBG.UDC? get_udc (string name);
        public USBG.Error create_gadget_vid_pid (string name, uint16 idVendor, uint16 idProduct, out unowned USBG.Gadget? g);
        public USBG.Error create_gadget (string name, USBG.GadgetAttributes? g_attrs, USBG.GadgetStrings? g_strs, out unowned USBG.Gadget? g);
        public unowned USBG.Gadget? get_first_gadget ();
        public unowned USBG.UDC? get_first_udc ();
    }

    [Compact]
	[CCode (cname = "usbg_gadget", cprefix = "usbg_", has_type_id = false)]
    public class Gadget {
        public unowned USBG.Function? get_function (USBG.FunctionType type, string instance);
        public unowned USBG.Config? get_config (int id, string label);
        [CCode (cname="usbg_rm_gadget")]
        public USBG.Error remove (USBG.RemoveOption options);
        [CCode (cname="usbg_rm_gadget_strs")]
        public USBG.Error remove_strings (int language);
        [CCode (cname="usbg_set_gadget_attr")]
        public USBG.Error set_attribute (USBG.Attribute attr, int val);
        [CCode (cname="usbg_get_gadget_attr")]
        public int get_attribute (USBG.Attribute attr);
        [CCode (cname="usbg_set_gadget_attrs")]
        public USBG.Error set_attributes (USBG.GadgetAttributes g_attrs);
        [CCode (cname="usbg_get_gadget_attrs")]
        public USBG.Error get_attributes (out USBG.GadgetAttributes g_attrs);
        [CCode (cname="usbg_get_gadget_name")]
        public unowned string get_name ();
        [CCode (cname="usbg_set_gadget_vendor_id")]
        public USBG.Error set_vendor_id (uint16 idVendor);
        [CCode (cname="usbg_set_gadget_product_id")]
        public USBG.Error set_product_id (uint16 idProduct);
        [CCode (cname="usbg_set_gadget_device_class")]
        public USBG.Error set_device_class (uint8 bDeviceClass);
        [CCode (cname="usbg_set_gadget_device_protocol")]
        public USBG.Error set_device_protocol (uint8 bDeviceProtocol);
        [CCode (cname="usbg_set_gadget_device_subclass")]
        public USBG.Error set_device_subclass (uint8 bDeviceSubClass);
        [CCode (cname="usbg_set_gadget_device_max_packet")]
        public USBG.Error set_device_max_packet (uint8 bMaxPacketSize0);
        [CCode (cname="usbg_set_gadget_device_bcd_device")]
        public USBG.Error set_device_bcd_device (uint16 bcdDevice);
        [CCode (cname="usbg_set_gadget_device_bcd_usb")]
        public USBG.Error set_device_bcd_usb (uint16 bcdUSB);
        [CCode (cname="usbg_get_gadget_strs")]
        public USBG.Error get_strings (int lang, out USBG.GadgetStrings g_strs);
        [CCode (cname="usbg_get_gadget_strs_langs")]
        public USBG.Error get_languages ([CCode (array_length = false)] out int[] langs);
        [CCode (cname="usbg_set_gadget_str")]
        public USBG.Error set_string (USBG.GadgetString str, int lang, string val);
        [CCode (cname="usbg_set_gadget_strs")]
        public USBG.Error set_strings (int lang, USBG.GadgetStrings g_strs);
        [CCode (cname="usbg_set_gadget_serial_number")]
        public USBG.Error set_serial_number (int lang, string serial);
        [CCode (cname="usbg_set_gadget_manufacturer")]
        public USBG.Error set_manufacturer (int lang, string mnf);
        [CCode (cname="usbg_set_gadget_product")]
        public USBG.Error set_product (int lang, string prd);
        [CCode (cname="usbg_get_gadget_os_descs")]
        public USBG.Error get_os_description (ref USBG.GadgetOSDescription g_os_descs);
        [CCode (cname="usbg_set_gadget_os_descs")]
        public USBG.Error set_os_description (USBG.GadgetOSDescription g_os_descs);
        public USBG.Error create_function (USBG.FunctionType g_os_descs, string instance, void* f_attrs, out unowned USBG.Function? f);
        public USBG.Error create_config (int id, string label, USBG.ConfigAttributes? c_attrs, USBG.ConfigStrings? c_strs, out unowned USBG.Config? c);
        [CCode (cname="usbg_get_os_desc_binding")]
        public unowned USBG.Config? get_os_description_config ();
        [CCode (cname="usbg_set_os_desc_config")]
        public USBG.Error set_os_description_config (USBG.Config c);
        [CCode (cname="usbg_enable_gadget")]
        public USBG.Error enable (USBG.UDC udc);
        [CCode (cname="usbg_disable_gadget")]
        public USBG.Error disable ();
        [CCode (cname="usbg_get_gadget_udc")]
        public unowned USBG.UDC? get_udc ();
        public unowned USBG.Function? get_first_function ();
        public unowned USBG.Config? get_first_config ();
        [CCode (cname="usbg_get_next_gadget")]
        public unowned USBG.Gadget? next ();
    }

    [Compact]
	[CCode (cname = "usbg_config", has_type_id = false)]
    public class Config {
        [CCode (cname="DEFAULT_CONFIG_LABEL")]
        public const string DEFAULT_LABEL;
        [CCode (cname="usbg_get_config_label")]
        public unowned string get_label ();
        [CCode (cname="usbg_get_config_id")]
        public int get_id ();
        [CCode (cname="usbg_set_config_attrs")]
        public USBG.Error set_attributes (USBG.ConfigAttributes c_attrs);
        [CCode (cname="usbg_get_config_attrs")]
        public USBG.Error get_attributes (out USBG.ConfigAttributes c_attrs);
        [CCode (cname="usbg_set_config_max_power")]
        public USBG.Error set_max_power (int bMaxPower);
        [CCode (cname="usbg_set_config_bm_attrs")]
        public USBG.Error set_bitmap_attributes (int bmAttributes);
        [CCode (cname="usbg_set_config_strs")]
        public USBG.Error set_strings (int lang, USBG.ConfigStrings c_strs);
        [CCode (cname="usbg_get_config_strs")]
        public USBG.Error get_strings (int lang, out USBG.ConfigStrings c_strs);
        [CCode (cname="usbg_get_config_strs_langs")]
        public USBG.Error get_languages ([CCode (array_length = false)] out int[] langs);
        [CCode (cname="usbg_set_config_string")]
        public USBG.Error set_string (int lang, string str);
        [CCode (cname="usbg_add_config_function")]
        public USBG.Error add_function (string name, USBG.Function f);
        [CCode (cname="usbg_rm_config")]
        public USBG.Error remove (USBG.RemoveOption options);
        [CCode (cname="usbg_rm_config_strs")]
        public USBG.Error remove_strings (int language);
        [CCode (cname="usbg_get_first_binding")]
        public unowned USBG.Binding? get_first_binding ();
        [CCode (cname="usbg_get_next_config")]
        public unowned USBG.Config? next ();
        
    }

    [Compact]
	[CCode (cname = "usbg_function", has_type_id = false)]
    public class Function {
        [CCode (cname="usbg_get_function_instance")]
        public unowned string get_instance ();
        [CCode (cname="usbg_get_function_type")]
        public USBG.FunctionType get_function_type ();
        [CCode (cname="usbg_get_function_attrs")]
        public USBG.Error get_attributes (void* f_attrs);
        [CCode (cname="usbg_set_function_attrs")]
        public USBG.Error set_attributes (void* f_attrs);
        [CCode (cname="usbg_get_interf_os_desc")]
        public USBG.Error get_os_description (string interface_name, out USBG.OSDescription f_os_desc);
        [CCode (cname="usbg_set_interf_os_desc")]
        public USBG.Error set_os_description (string interface_name, USBG.OSDescription f_os_desc);
        [CCode (cname="usbg_get_next_function")]
        public unowned USBG.Function? next ();
        [CCode (cname="usbg_rm_function")]
        public USBG.Error remove (USBG.RemoveOption options);
    }

    [Compact]
	[CCode (cname = "usbg_binding", cprefix = "usbg_", has_type_id = false)]
    public class Binding {
        [CCode (cname="usbg_get_binding_target")]
        public unowned USBG.Function get_target ();
        [CCode (cname="usbg_get_binding_name")]
        public unowned string get_name ();
        [CCode (cname="usbg_get_next_binding")]
        public unowned USBG.Binding? next ();
        [CCode (cname="usbg_rm_binding")]
        public USBG.Error remove ();
    }

    [Compact]
	[CCode (cname = "usbg_udc", has_type_id = false)]
    public class UDC {
        [CCode (cname="usbg_get_udc_name")]
        public unowned string get_name ();
        [CCode (cname="usbg_get_udc_gadget")]
        public unowned USBG.Gadget? get_gadget ();
        [CCode (cname="usbg_get_next_udc")]
        public unowned USBG.UDC? next ();
        
    }

	[CCode (cname = "usbg_gadget_attr", cprefix = "USBG_", has_type_id = false)]
    public enum Attribute {
	    BCD_USB,
	    B_DEVICE_CLASS,
	    B_DEVICE_SUB_CLASS,
	    B_DEVICE_PROTOCOL,
	    B_MAX_PACKET_SIZE_0,
	    ID_VENDOR,
	    ID_PRODUCT,
	    BCD_DEVICE;
        [CCode (cname="usbg_get_gadget_attr_str")]
        public unowned string to_string ();
        [CCode (cname="usbg_lookup_gadget_attr")]
        public static USBG.Attribute from_string (string name);
    }

	[CCode (cname = "struct usbg_gadget_attrs", destroy_function = "", has_type_id = false)]
    public struct GadgetAttributes {
	    uint16 bcdUSB;
	    uint8 bDeviceClass;
	    uint8 bDeviceSubClass;
	    uint8 bDeviceProtocol;
	    uint8 bMaxPacketSize0;
	    uint16 idVendor;
	    uint16 idProduct;
	    uint16 bcdDevice;
    }

	[CCode (cname = "usbg_gadget_str", cprefix = "USBG_STR_", has_type_id = false)]
    public enum GadgetString {
	    MANUFACTURER,
	    PRODUCT,
	    SERIAL_NUMBER;
        [CCode (cname="usbg_get_gadget_str_name")]
        public unowned string to_string ();
        [CCode (cname="usbg_lookup_gadget_str")]
        public static USBG.GadgetString from_string (string name);
    }

	[CCode (cname = "struct usbg_gadget_strs", destroy_function="usbg_free_gadget_strs", has_type_id = false)]
    public struct GadgetStrings {
	    string manufacturer;
	    string product;
	    string serial;
    }

	[CCode (cname = "struct usbg_gadget_os_descs", destroy_function="usbg_free_gadget_os_desc", has_type_id = false)]
    public struct GadgetOSDescription {
	    bool use;
	    uint8 b_vendor_code;
	    string qw_sign;
    }

	[CCode (cname = "usbg_gadget_os_desc_strs", cprefix = "OS_DESC_", has_type_id = false)]
    public enum GadgetOsDesctiptionString {
	    USE,
	    B_VENDOR_CODE,
	    QW_SIGN;
        [CCode (cname="usbg_get_gadget_os_desc_name")]
        public unowned string to_string ();
    }

	[CCode (cname = "struct usbg_config_attrs", destroy_function = "", has_type_id = false)]
    public struct ConfigAttributes {
	    uint8 bmAttributes;
	    uint8 bMaxPower;
    }

	[CCode (cname = "struct usbg_config_strs", destroy_function="usbg_free_config_strs", has_type_id = false)]
    public struct ConfigStrings {
	    string configuration;
    }

	[CCode (cname = "usbg_function_type", cprefix = "USBG_F_", has_type_id = false)]
    public enum FunctionType {
	    SERIAL,
	    ACM,
	    OBEX,
	    ECM,
	    SUBSET,
	    NCM,
	    EEM,
	    RNDIS,
	    PHONET,
	    FFS,
	    MASS_STORAGE,
	    MIDI,
	    LOOPBACK,
	    HID,
	    UAC2;
        [CCode (cname="usbg_get_function_type_str")]
        public unowned string to_string ();
        [CCode (cname="usbg_lookup_function_type")]
        public static USBG.FunctionType from_string (string name);
    }

	[CCode (cname = "struct usbg_function_os_desc", destroy_function="usbg_free_interf_os_desc", has_type_id = false)]
    public struct OSDescription {
	    string compatible_id;
	    string sub_compatible_id;
    }

	[CCode (cname = "usbg_error", cprefix = "USBG_ERROR_", has_type_id = false)]
    public enum Error {
        [CCode (cname="USBG_SUCCESS")]
	    SUCCESS,
	    NO_MEM,
	    NO_ACCESS,
	    INVALID_PARAM,
	    NOT_FOUND,
	    IO,
	    EXIST,
	    NO_DEV,
	    BUSY,
	    NOT_SUPPORTED,
	    PATH_TOO_LONG,
	    INVALID_FORMAT,
	    MISSING_TAG,
	    INVALID_TYPE,
	    INVALID_VALUE,
	    NOT_EMPTY,
	    OTHER_ERROR;
        [CCode (cname = "usbg_error_name")]
        public unowned string name ();
        [CCode (cname = "usbg_strerror")]
        public unowned string description ();
    }

	[CCode (cprefix = "USBG_RM_", has_type_id = false)]
    public enum RemoveOption {
        [CCode (cname="0")]
	    NONE = 0,
	    RECURSE
    }

    [CCode (cname = "LANG_US_ENG")]
    public const int LANG_US_ENG;
    [CCode (cname = "USBG_VERSION")]
    public const int VERSION;
    [CCode (cname = "USBG_MAKE_VERSION")]
    public static int make_version (int major, int minor, int micro);
}
