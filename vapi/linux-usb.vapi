namespace Linux {
    [SimpleType]
    [CCode (cname = "__le32", cheader_filename = "linux/types.h")]
    public struct le32 : uint32 {
        
    }
    [SimpleType]
    [CCode (cname = "__u8", cheader_filename = "linux/types.h")]
    public struct u8 : uint8 {
        
    }
    [SimpleType]
    [CCode (cname = "__le16", cheader_filename = "linux/types.h")]
    public struct le16 : uint16 {
        
    }

    [CCode (cname = "htole16", cheader_filename = "endian.h")]
    public uint16 htole16 (uint16 host_16bits);
    [CCode (cname = "htole32", cheader_filename = "endian.h")]
    public uint32 htole32 (uint32 host_32bits);

    [CCode (cname = "le16toh", cheader_filename = "endian.h")]
    public uint16 le16toh (uint16 little_endian_16bits);
    [CCode (cname = "le32toh", cheader_filename = "endian.h")]
    public uint32 le32toh (uint32 little_endian_32bits);
}

namespace Linux.Usb {

    [CCode (cprefix = "USB_DIR_", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public enum Direction {
        OUT,
        IN
    }

    [CCode (cprefix = "USB_CLASS_", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public enum Class {
        PER_INTERFACE,
        AUDIO,
        COMM,
        HID,
        PHYSICAL,
        STILL_IMAGE,
        PRINTER,
        MASS_STORAGE,
        HUB,
        CDC_DATA,
        CSCID,
        CONTENT_SEC,
        VIDEO,
        WIRELESS_CONTROLLER,
        MISC,
        APP_SPEC,
        VENDOR_SPEC,
    }

    [CCode (cprefix = "USB_DT_", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public enum DescriptorType {
        DEVICE,
        CONFIG,
        STRING,
        INTERFACE,
        ENDPOINT,
        DEVICE_QUALIFIER,
        OTHER_SPEED_CONFIG,
        INTERFACE_POWER,
        OTG,
        DEBUG,
        INTERFACE_ASSOCIATION,
        SECURITY,
        KEY,
        ENCRYPTION_TYPE,
        BOS,
        DEVICE_CAPABILITY,
        WIRELESS_ENDPOINT_COMP,
        WIRE_ADAPTER,
        RPIPE,
        CS_RADIO_CONTROL,
        PIPE_USAGE,
        SS_ENDPOINT_COMP,

        INTERFACE_SIZE,
        ENDPOINT_SIZE,
        ENDPOINT_AUDIO_SIZE,
        SS_EP_COMP_SIZE
    }

    [CCode (cname = "struct usb_ctrlrequest", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public struct CtrlRequest {
	    Linux.u8 bRequestType;
	    Linux.u8 bRequest;
	    Linux.le16 wValue;
	    Linux.le16 wIndex;
	    Linux.le16 wLength;
    }

    [Flags]
    [CCode (cprefix = "USB_ENDPOINT_", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public enum Endpoint {
        NUMBER_MASK,
        DIR_MASK,

        XFERTYPE_MASK,
        XFER_CONTROL,
        XFER_ISOC,
        XFER_BULK,
        XFER_INT,
        MAX_ADJUSTABLE,

        SYNCTYPE,
        SYNC_NONE,
        SYNC_ASYNC,
        SYNC_ADAPTIVE,
        SYNC_SYNC,

        USAGE_MASK,
        USAGE_DATA,
        USAGE_FEEDBACK,
        USAGE_IMPLICIT_FB
    }

    [CCode (cname = "struct usb_ss_ep_comp_descriptor", has_type_id = false, cheader_filename = "linux/usb/ch9.h")]
    public struct SSEpCompDescriptor {
        Linux.u8  bLength;
        Linux.u8  bDescriptorType;

        Linux.u8  bMaxBurst;
        Linux.u8  bmAttributes;
        Linux.le16 wBytesPerInterval;
    }

    [CCode (cname = "struct usb_interface_descriptor", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
    public struct InterfaceDescriptor {
        Linux.u8  bLength;
        Linux.u8  bDescriptorType;

        Linux.u8  bInterfaceNumber;
        Linux.u8  bAlternateSetting;
        Linux.u8  bNumEndpoints;
        Linux.u8  bInterfaceClass;
        Linux.u8  bInterfaceSubClass;
        Linux.u8  bInterfaceProtocol;
        Linux.u8  iInterface;
        public static InterfaceDescriptor new_vendor_spec () {
            return Linux.Usb.InterfaceDescriptor () {
                bLength = (Linux.u8) Linux.Usb.DescriptorType.INTERFACE_SIZE,
                bDescriptorType = Linux.Usb.DescriptorType.INTERFACE,
                bNumEndpoints = 2,
                bInterfaceClass = Linux.Usb.Class.VENDOR_SPEC,
                iInterface = 1
            };
        }
    }

    [CCode (cname = "struct usb_endpoint_descriptor_no_audio", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
    public struct EndpointDescriptorNoAudio {
        Linux.u8 bLength;
        Linux.u8 bDescriptorType;

        Linux.u8 bEndpointAddress;
        Linux.u8 bmAttributes;
        Linux.le16 wMaxPacketSize;
        Linux.u8 bInterval;
        public static EndpointDescriptorNoAudio new_sink () {
            return Linux.Usb.EndpointDescriptorNoAudio () {
                bLength = (Linux.u8) Linux.Usb.DescriptorType.ENDPOINT_SIZE,
                bDescriptorType = Linux.Usb.DescriptorType.ENDPOINT,
                bEndpointAddress = 1 | Linux.Usb.Direction.IN,
                bmAttributes = Linux.Usb.Endpoint.XFER_BULK
            };
        }
        public static EndpointDescriptorNoAudio new_source () {
            return Linux.Usb.EndpointDescriptorNoAudio () {
                bLength = (Linux.u8) Linux.Usb.DescriptorType.ENDPOINT_SIZE,
                bDescriptorType = Linux.Usb.DescriptorType.ENDPOINT,
                bEndpointAddress = 2 | Linux.Usb.Direction.OUT,
                bmAttributes = Linux.Usb.Endpoint.XFER_BULK
            };
        }
    }

    [CCode (cname = "struct usb_endpoint_descriptor", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
    public struct EndpointDescriptor {
        Linux.u8 bLength;
        Linux.u8 bDescriptorType;

        Linux.u8 bEndpointAddress;
        Linux.u8 bmAttributes;
        Linux.le16 wMaxPacketSize;
        Linux.u8 bInterval;

        /* NOTE:  these two are _only_ in audio endpoints. */
        /* use USB_DT_ENDPOINT*_SIZE in bLength, not sizeof. */
        Linux.u8 bRefresh;
        Linux.u8 bSynchAddress;
    }

    namespace FunctionFS {
        [CCode (cname = "enum usb_functionfs_event_type", cprefix = "FUNCTIONFS_", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
        public enum EventType {
	        BIND,
	        UNBIND,
	        ENABLE,
	        DISABLE,
	        SETUP,
	        SUSPEND,
	        RESUME
        }

        [CCode (cname = "struct usb_functionfs_event", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
        public struct Event {
            [CCode (cname = "u.setup")]
            Linux.Usb.CtrlRequest setup;
            Linux.Usb.FunctionFS.EventType type;
            Linux.u8 pad[3];
        }

        [CCode (cname = "__le32", has_type_id = false, cheader_filename = "linux/types.h")]
        public enum Magic {
            [CCode (cname = "FUNCTIONFS_DESCRIPTORS_MAGIC", cheader_filename = "linux/usb/functionfs.h")]
            DESCRIPTORS,
            [CCode (cname = "FUNCTIONFS_STRINGS_MAGIC", cheader_filename = "linux/usb/functionfs.h")]
            STRINGS,
            [CCode (cname = "FUNCTIONFS_DESCRIPTORS_MAGIC_V2", cheader_filename = "linux/usb/functionfs.h")]
            DESCRIPTORS_V2
        }

        [Flags]
        [CCode (cname = "functionfs_flags", cprefix = "FUNCTIONFS_", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
        public enum Flags {
            HAS_FS_DESC,
            HAS_HS_DESC,
            HAS_SS_DESC,
            HAS_MS_OS_DESC,
            VIRTUAL_ADDR,
            EVENTFD,
            ALL_CTRL_RECIP,
            CONFIG0_SETUP,
        }

        [CCode (cname = "struct usb_functionfs_descs_head_v2", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
        public struct DescsHeadV2 {
            Linux.Usb.FunctionFS.Magic magic;
            Linux.le32 length;
            Linux.le32 flags;
        }

        [CCode (cname = "struct usb_functionfs_strings_head", has_type_id = false, cheader_filename = "linux/usb/functionfs.h")]
        public struct StringsHead {
            Linux.Usb.FunctionFS.Magic magic;
            Linux.le32 length;
            Linux.le32 str_count;
            Linux.le32 lang_count;
        }
    }
}
