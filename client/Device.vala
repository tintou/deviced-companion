/* Device.vala
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

public class DevicedCompanionClient.Device : GLib.Object {
    public unowned LibUSB.Device usb_device { get; construct; }
    private Cancellable cancellable;
    private LibUSB.DeviceHandle handle;
    private GLib.Queue<Transfer> transfers;
    private Transfer? current_transfer = null;

    private unowned LibUSB.EndpointDescriptor in_endpoint;
    private unowned LibUSB.EndpointDescriptor out_endpoint;

    public Device (LibUSB.Device usb_device) {
        Object (usb_device: usb_device);
    }

    construct {
        cancellable = new GLib.Cancellable ();
        transfers = new GLib.Queue<Transfer> ();
        LibUSB.DeviceDescriptor desc;
        usb_device.get_device_descriptor (out desc);

        LibUSB.ConfigDescriptor config;
        usb_device.get_active_config_descriptor (out config);
        foreach (unowned LibUSB.Interface intf in config.@interface) {
            foreach (unowned LibUSB.InterfaceDescriptor int_desc in intf.altsetting) {
                if (LibUSB.ClassCode.VENDOR_SPEC == int_desc.bInterfaceClass) {
                    write_to_interface (int_desc);
                }
            }
        }

        /*uint8[] data = new uint8[100];
        handle.get_string_descriptor_ascii (desc.iManufacturer, data);
        warning ("%s", (string)data);*/
    }

    private void write_to_interface (LibUSB.InterfaceDescriptor int_desc) {
        if (usb_device.open (out handle) != LibUSB.Error.SUCCESS) {
            warning ("ERROR");
            return;
        }

        handle.claim_interface (int_desc.bInterfaceNumber);
        foreach (unowned LibUSB.EndpointDescriptor endpoint in int_desc.endpoint) {
            if (endpoint.get_direction () == LibUSB.EndpointDirection.OUT) {
                out_endpoint = endpoint;
            } else if (endpoint.get_direction () == LibUSB.EndpointDirection.IN) {
                in_endpoint = endpoint;
            }
        }

        //while (!cancellable.is_cancelled ()) {
            var transfer = new Transfer (handle, out_endpoint);
            transfer.set_buffer ("Ceci est un test".data);
            transfers.push_tail (transfer);
            transfer = new Transfer (handle, in_endpoint);
            transfer.input = true;
            transfers.push_tail (transfer);
            start_transfer.begin ();
        //}

        handle.release_interface (int_desc.bInterfaceNumber);
    }

    private async void start_transfer () {
        if (current_transfer != null)
            return;

        current_transfer = transfers.pop_head ();
        if (current_transfer == null)
            return;

        current_transfer.finished.connect (() => {
            current_transfer = null;
            start_transfer ();
        });

        current_transfer.submit ();
    }
}
