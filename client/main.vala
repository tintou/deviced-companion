/* main.vala
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

public class DevicedCompanionClient.Application : GLib.Application {
    GLib.Array<Device> devices;
    GLib.Cancellable cancellable;

    public Application () {
        Object (
            application_id: "org.gnome.deviced-companion-client",
            flags: ApplicationFlags.FLAGS_NONE
        );

        devices = new GLib.Array<Device> ();
        cancellable = new GLib.Cancellable ();
    }

    ~Application () {
        cancellable.cancel ();
    }

    public override void activate () {
        LibUSB.Context context;
        if (LibUSB.Context.init (out context) != LibUSB.Error.SUCCESS) {
            warning ("ERROR");
            return;
        }

        new Thread<void*> ("LibUSB event thread", () => {
            while (!cancellable.is_cancelled ()) {
                context.handle_events ();
            }

            return null;
        });

        LibUSB.Device[] usb_device_list;
        context.get_device_list (out usb_device_list);
        foreach (var usb_device in usb_device_list) {
            LibUSB.DeviceDescriptor desc;
            usb_device.get_device_descriptor (out desc);
            if (desc.idVendor == 0x04e8 && desc.idProduct == 0xe1ce) {
                var device = new Device (usb_device);
                devices.append_val (device);
            }
        }

        hold ();
    }
}

public int main (string[] args) {
    var app = new DevicedCompanionClient.Application ();
    return app.run (args);
}
