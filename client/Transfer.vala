/* Transfer.vala
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

public class DevicedCompanionClient.Transfer : GLib.Object {
    LibUSB.Transfer transfer;
    public bool input = false;
    public signal void finished ();

    public Transfer (LibUSB.DeviceHandle handle, LibUSB.EndpointDescriptor endpoint) {
        uint8[] buffer = new uint8[256];
        transfer = LibUSB.Transfer.new_bulk (handle, endpoint.bEndpointAddress, buffer, transfer_cb);
    }

    ~Transfer () {
        transfer.cancel ();
    }

    public void set_buffer (uint8[] data) {
        transfer.fill_bulk_transfer (transfer.dev_handle, transfer.endpoint, data, transfer_cb);
    }

    public void submit () throws GLib.Error {
        var error = transfer.submit ();
        if (error != LibUSB.Error.SUCCESS) {
            throw new GLib.IOError.FAILED ("%s: %s", error.get_name (), error.get_description ());
        }
    }

    private void transfer_cb (LibUSB.Transfer returned_transfer) {
        var instance = returned_transfer.user_data as DevicedCompanionClient.Transfer;
        warning("Finished: %s", returned_transfer.status.to_string());
        if (instance.input) {
            var string_builder = new GLib.StringBuilder ();
            string_builder.prepend_len ((string)returned_transfer.buffer, returned_transfer.actual_length);
            warning ("%s", string_builder.str);
        }

        instance.transfer_cb_real ();
    }

    private void transfer_cb_real () {
        Idle.add (() => {
            finished ();
            return GLib.Source.REMOVE;
        });
    }
}
