/* Structs.vapi
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

namespace Structs {
    public struct SDescs {
        Linux.Usb.InterfaceDescriptor intf;
        Linux.Usb.EndpointDescriptorNoAudio sink;
        Linux.Usb.EndpointDescriptorNoAudio source;
        public static SDescs get_vendor_with_sink_source () {
            return SDescs () {
                intf = Linux.Usb.InterfaceDescriptor.new_vendor_spec (),
                sink = Linux.Usb.EndpointDescriptorNoAudio.new_sink (),
                source = Linux.Usb.EndpointDescriptorNoAudio.new_source ()
            };
        }
    }

    public struct Descriptors {
        Linux.Usb.FunctionFS.DescsHeadV2 header;
        Linux.le32 fs_count;
        Linux.le32 hs_count;
        SDescs fs_descs;
        SDescs hs_descs;
    }

    public struct StringTab {
        Linux.le16 lang_code;
        string[] strings;
    }

    public struct Strings {
        Linux.Usb.FunctionFS.StringsHead header;
        StringTab[] stringtab;
    }

    [CCode(cname = "descriptors", cheader_filename = "structs.h")]
    public static Descriptors descriptors;
    [CCode(cname = "sizeof(descriptors)", cheader_filename = "structs.h")]
    public static int descriptors_size;
    [CCode(cname = "strings", cheader_filename = "structs.h")]
    public static Strings strings;
    [CCode(cname = "sizeof(strings)", cheader_filename = "structs.h")]
    public static int strings_size;
}
