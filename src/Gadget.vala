/* Gadget.vala
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

public class DevicedCompanion.Gadget : GLib.Object {
    public unowned USBG.Gadget usbg_gadget { get; construct; }

    private const string CONFIG_NAME = "config_1";
    private const string FUNCTION_NAME_1 = "this_dev_name";

    private string? mount_dir = null;
    private int fd_ep0 = 0;
    private int fd_ep1 = 0;
    private int fd_ep2 = 0;

    private GLib.Cancellable cancellable = null;

    public Gadget (USBG.Gadget usbg_gadget) throws GLib.Error {
        Object (usbg_gadget: usbg_gadget);

        USBG.ConfigStrings c_strs = USBG.ConfigStrings () {
            configuration = "1xFFS"
        };

        unowned USBG.Config? config = usbg_gadget.get_config (1, CONFIG_NAME);
        if (config == null) {
            var error = usbg_gadget.create_config (1, CONFIG_NAME, null, c_strs, out config);
            if (error != USBG.Error.SUCCESS) {
                throw new GLib.IOError.FAILED ("%s: %s", error.name (), error.description ());
            }
        }

        unowned USBG.Function? ffs1 = usbg_gadget.get_function (USBG.FunctionType.FFS, FUNCTION_NAME_1);
        if (ffs1 == null) {
            var error = usbg_gadget.create_function (USBG.FunctionType.FFS, FUNCTION_NAME_1, null, out ffs1);
            if (error != USBG.Error.SUCCESS) {
                throw new GLib.IOError.FAILED ("%s: %s", error.name (), error.description ());
            } else {
                error = config.add_function ("some_name_here", ffs1);
                if (error != USBG.Error.SUCCESS) {
                    throw new GLib.IOError.FAILED ("%s: %s", error.name (), error.description ());
                }
            }
        }

        mount_dir = GLib.DirUtils.make_tmp ("ffs-XXXXXX");
        warning (mount_dir);
        if (Linux.mount (FUNCTION_NAME_1, mount_dir, "functionfs") != 0) {
            throw new GLib.IOError.FAILED ("%d: %s", GLib.errno, Posix.strerror (Posix.errno));
        }

        fd_ep0 = Posix.open (GLib.Path.build_filename (mount_dir, "ep0"), Posix.O_RDWR);
        var size = Posix.write (fd_ep0, &Structs.descriptors, Structs.descriptors_size);
        if (size < 0) {
            throw new GLib.IOError.FAILED ("%d: %s", GLib.errno, Posix.strerror (Posix.errno));
        }

        size = Posix.write (fd_ep0, &Structs.strings, Structs.strings_size);
        if (size < 0) {
            throw new GLib.IOError.FAILED ("%d: %s", GLib.errno, Posix.strerror (Posix.errno));
        }

        fd_ep1 = Posix.open (GLib.Path.build_filename (mount_dir, "ep1"), Posix.O_RDWR);
        if (fd_ep1 < 0) {
            critical ("Unable to open ep1");
        }

        fd_ep2 = Posix.open (GLib.Path.build_filename (mount_dir, "ep2"), Posix.O_RDWR);
        if (fd_ep2 < 0) {
            critical ("Unable to open ep2");
        }

        message ("Gadget \"%s\" created", usbg_gadget.get_name ());
    }

    ~Gadget () {
        cancellable.cancel ();
        if (fd_ep0 > 0 && Posix.close (fd_ep0) < 0) {
            critical (Posix.strerror (Posix.errno));
        }

        if (fd_ep1 > 0 && Posix.close (fd_ep1) < 0) {
            critical (Posix.strerror (Posix.errno));
        }

        if (fd_ep2 > 0 && Posix.close (fd_ep2) < 0) {
            critical (Posix.strerror (Posix.errno));
        }

        if (mount_dir != null) {
            Linux.umount2 (mount_dir, Linux.UnmountFlags.FORCE);
        }
    }

    public void enable (USBG.UDC udc) throws GLib.Error {
        var error = usbg_gadget.enable (udc);
        if (error != USBG.Error.SUCCESS) {
            throw new GLib.IOError.FAILED ("%s: %s", error.name (), error.description ());
        }

        message ("Gadget \"%s\" enabled", usbg_gadget.get_name ());

        while (true) {
            var event = Linux.Usb.FunctionFS.Event ();
            if (Posix.read (fd_ep0, &event, sizeof(Linux.Usb.FunctionFS.Event)) > 0) {
                critical ("%d", event.type);
                if (event.type == Linux.Usb.FunctionFS.EventType.ENABLE)
                    break;
            }
        }

        start_read_write_loop.begin ();
    }

    public void disable () {
        var error = usbg_gadget.disable ();
        if (error != USBG.Error.SUCCESS) {
            critical ("%s: %s", error.name (), error.description ());
        }

        message ("Gadget \"%s\" disabled", usbg_gadget.get_name ());
    }

    private async void start_read_write_loop () {
        new Thread<void*> (null, () => {
            while (!cancellable.is_cancelled ()) {
                read ();
            }

            return null;
        });
        new Thread<void*> (null, () => {
            while (!cancellable.is_cancelled ()) {
                write ();
            }

            return null;
        });
    }

    private void read () {
        uint8[] buffer = new uint8[1024];
        critical ("Reading...");
        ssize_t bytes_read = Posix.read (fd_ep2, buffer, buffer.length);
        critical ("Read: %ld bytes: %s", bytes_read, (string)buffer);
    }

    private void write () {
        uint8[] buffer = "hello back".data;
        critical ("Writing...");
        var size = Posix.write (fd_ep1, buffer, buffer.length);
        if (size < 0) {
           critical ("%d: %s", GLib.errno, Posix.strerror (Posix.errno));
        }
    }
}
