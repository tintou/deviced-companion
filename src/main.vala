/* main.vala
 *
 * Copyright 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors: Corentin Noël <corentin.noel@collabora.com>
 */

public class DevicedCompanion.Application : GLib.Application {
    public const string CONFIGFS_PATH = "/sys/kernel/config";
    public const uint16 ID_VENDOR = 0x04e8;
    public const uint16 ID_PRODUCT = 0xe1ce;
    private unowned USBG.State? state = null;
    private GLib.Array<Gadget> gadgets;

    public Application () {
        Object (
            application_id: "org.gnome.deviced-companion",
            flags: ApplicationFlags.FLAGS_NONE
        );

        gadgets = new GLib.Array<Gadget> ();
    }

    ~Application () {
        foreach (var gadget in gadgets.data) {
            var error = gadget.usbg_gadget.remove (USBG.RemoveOption.RECURSE);
            if (error != USBG.Error.SUCCESS) {
                critical (error.description ());
            }
        }

        if (state != null) {
            state.cleanup ();
        }
    }

    public override void activate () {
        var error = USBG.State.init (CONFIGFS_PATH, out state);
        if (error != USBG.Error.SUCCESS) {
            critical (error.description ());
            return;
        }

        unowned USBG.UDC? udc;
        for (udc = state.get_first_udc (); udc != null; udc = udc.next ()) {
            create_gadget (udc);
        }

        char carac = 0;
        while (carac != 'q') {
            stdin.scanf ("%c", out carac);
        }
    }

    public void create_gadget (USBG.UDC udc) {
        USBG.GadgetAttributes g_attrs = USBG.GadgetAttributes () {
            bcdUSB = 0x0200,
            bDeviceClass = Linux.Usb.Class.PER_INTERFACE,
            bDeviceSubClass = 0x0,
            bDeviceProtocol = 0x0,
            bMaxPacketSize0 = 64, /* max allowed ep0 packet size */
            idVendor = ID_VENDOR,
            idProduct = ID_PRODUCT,
            bcdDevice = 0x0001 /* version of device */
        };

        USBG.GadgetStrings g_strs = USBG.GadgetStrings () {
            serial = "0123456789", /* serial number */
            manufacturer = "Foo Inc.",
            product = "Bar Gadget"
        };

        unowned USBG.Gadget? _usbg_gadget = state.get_gadget ("g1");
        if (_usbg_gadget != null) {
            _usbg_gadget.disable ();
            _usbg_gadget.remove (USBG.RemoveOption.RECURSE);
            _usbg_gadget = null;

        }

        var error = state.create_gadget ("g1", g_attrs, g_strs, out _usbg_gadget);
        if (error != USBG.Error.SUCCESS) {
            critical (error.description ());
            return;
        }

        try {
            var gadget = new Gadget (_usbg_gadget);
            gadget.enable (udc);
            gadgets.append_val (gadget);
        } catch (Error e) {
            critical (e.message);
        }
    }
}

public int main (string[] args) {
    var app = new DevicedCompanion.Application ();
    return app.run (args);
}
