# Deviced Companion

USB daemon on the device and host utility made to be used with Deviced.

The daemon uses the Linux kernel API.

## Building and Installation

You'll need the following dependencies:
* libglib-2.0-dev
* meson
* valac

Specific to the client:
* libusb-1.0-dev

Specific to the gadget:
* [libusbgx](git@github.com:libusbgx/libusbgx.git)

By default, both client and gadget are built.

You can disable the client compilation by giving the `-Dclient=false` argument at configuration time.

You can disable the gadget compilation by giving the `-Dgadget=false` argument at configuration time.

Run `meson build` to configure the build environment. Change to the build directory and run `ninja` to build

    meson build --prefix=/usr
    cd build
    ninja

To install, use `ninja install`

    sudo ninja install

## On the device

On the device side, ensure that the `libcomposite` kernel module is loaded and that the `FunctionFS` filesystem is available in your kernel.

To start the daemon simply run the `deviced-companion` executable built from the `/src` directory

Note: Not all devices are capable to run as an USB Gadget, to know if the device has Gadget capabilities, see if there is an USB Device Controller in `/sys/class/udc/`. If this folder is empty, your device can't act as a gadget. You can still add an emulated UDC by loading the `dummy_hdc` module.

## On the host

To start the client sample application, simply run the `deviced-companion-client` executable built from the `/client` directory

